import { BrowserRouter, Route, Switch } from "react-router-dom"
import Main from "./Main"
import ListView from './ListView.js';

function App() {
  return (
    <BrowserRouter>
      <Switch>
        <Route 
          path='/list/:slug' 
          render={(props) => {
            return <ListView {...props}/>;
          }} 
        />
        <Route path='/'>
          <Main/>
        </Route>
      </Switch>
    </BrowserRouter>
  )
}

export default App