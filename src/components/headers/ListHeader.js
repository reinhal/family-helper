import { Header, Icon } from 'semantic-ui-react';
import '../../App.css'

const ListHeader = () => {
  return (
    <Header as="h1" textAlign='center' icon className="mt-1 mb-3">
      <Icon name='users' />
      <Header.Content>Family Helper</Header.Content>
    </Header>
  )
}

export default ListHeader