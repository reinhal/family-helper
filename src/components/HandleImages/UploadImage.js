import { useState, useRef} from 'react';
import { Header, Image, Button } from 'semantic-ui-react';
import '../../App.css';

const UploadImage = ({ getSelectedFile }) => {
  const inputRef = useRef()
  const [image, setImage] = useState("https://react.semantic-ui.com/images/wireframe/image.png")
  
  function handleInputChange(e) {
    e.preventDefault()
    const fileToUpload = e.target.files[0];
    if (!fileToUpload) return;
    const fileSampleUrl = URL.createObjectURL(fileToUpload)
    setImage(fileSampleUrl)
    getSelectedFile(fileToUpload)
  }

  return (
    <>
      <Header as="h4">Upload Your Image</Header>
      <Image size="large" src={image} />
      <input 
        ref={inputRef}
        className="hide" 
        type='file' 
        onChange={handleInputChange}
      />
      <Button 
        onClick={() => inputRef.current.click()} 
        className="mt-3"
      >Upload Image
      </Button>
    </>
  )
}

export default UploadImage