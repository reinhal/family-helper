import React, { useState } from 'react';
import {API, graphqlOperation} from 'aws-amplify';
import { 
  Button, 
  Modal, 
  Form 
} from 'semantic-ui-react';
import { createList, updateList } from '../../graphql/mutations';
import { useS3 } from '../../Hooks/useS3';
import UploadImage from '../HandleImages/UploadImage';

const ListModal = ({state, dispatch, saveList }) => {
  const [uploadToS3] = useS3();
  const [fileToUpload, setFileToUpload] = useState()

  async function saveList() {
    const imageKey= uploadToS3(fileToUpload);
    console.log('image key', imageKey)
    const { title, description } = state;
    const res = await API.graphql(
      graphqlOperation(createList, { input: { title, description, imageKey } })
    );
    dispatch({type: 'CLOSE_MODAL'})
    console.log('Save data with result:', res)
  }

  async function updateListById() {
    const {id, title, description} = state;
    const res = await API.graphql(
      graphqlOperation(updateList, { input: { id, title, description } })
    );
    dispatch({type: 'CLOSE_MODAL'})
    console.log('Edit data with result:', res)
  }

  function getSelectedFile(fileName) {
    setFileToUpload(fileName)
  }

  return (
    <>
      <Modal open={state.isModalOpen} dimmer='blurring'>
      <Modal.Header>
        { state.modalType === 'add'? 'Create ' : 'Edit '}
        Your List
      </Modal.Header>
      <Modal.Content>
        <Form>
          <Form.Input 
            error={
              true ? false : {content: "Please add a title for your list"}
            } 
            label="List Title" 
            placeholder="My new list"
            value={state.title}
            onChange={(e) => dispatch({ type: 'TITLE_CHANGED', value: e.target.value})}
          />
          <Form.TextArea 
            label="Description" 
            placeholder="list items" 
            value={state.description}
            onChange={(e) => dispatch({ type: 'DESCRIPTION_CHANGED', value: e.target.value})}
          ></Form.TextArea>
          <UploadImage getSelectedFile={getSelectedFile}/>
        </Form>
      </Modal.Content>
      <Modal.Actions>
        <Button negative onClick={() => dispatch({type: 'CLOSE_MODAL'})}>Cancel</Button>
        <Button positive onClick={state.modalType === 'add'? saveList : updateListById}>
        { state.modalType === 'add'? 'Create' : 'Update' }
        </Button>
      </Modal.Actions>
    </Modal>
  </>
  )
}

export default ListModal