import { useEffect, useReducer } from 'react';
import Amplify, {API, graphqlOperation} from 'aws-amplify';
import { AmplifyAuthenticator, AmplifySignOut } from '@aws-amplify/ui-react';
import awsConfig from './aws-exports';
import 'semantic-ui-css/semantic.min.css'
import { 
  Container, 
  Button, 
  Icon, 
} from 'semantic-ui-react'
import './App.css';
import MainHeader from './components/headers/MainHeader'
import Lists from './components/Lists/Lists'
import ListModal from './components/modals/ListModal'
import { listLists } from './graphql/queries';
import { deleteList } from './graphql/mutations';
import { 
  onCreateList, 
  onDeleteList, 
  onUpdateList 
} from './graphql/subscriptions';

Amplify.configure(awsConfig);

const initialState = {
  id: '',
  title: '',
  description: '',
  lists: [],
  isModalOpen: false,
  modalType: ''
}

function listReducer(state = initialState, action) {
  let newLists
  switch (action.type) {
    case 'DESCRIPTION_CHANGED':
      return {...state, description: action.value}
    case 'TITLE_CHANGED':
      return {...state, title: action.value}
    case 'UPDATE_LISTS':
      return {...state, lists: [...action.value, ...state.lists]}
    case 'OPEN_MODAL':
      return {...state, isModalOpen: true, modalType: 'add'}
    case 'CLOSE_MODAL':
      return {...state, isModalOpen: false, title: '', description: '', id: ''}
    case 'DELETE_LIST':
      console.log(action.value)
      deleteListById(action.value);
      return {...state}
    case 'DELETE_LIST_RESULT':
      newLists = state.lists.filter(item => item.id !== action.value)
      return { ...state, lists: newLists}
    case 'UPDATE_LIST_RESULT':
      const index = state.lists.findIndex(item => item.id === action.value.id)
      newLists = [...state.lists]
      delete action.value.listItems;
      newLists[index] = action.value
      return {...state, lists: newLists}
    case 'EDIT_LIST':
      const newValues = {...action.value}
      delete newValues.children
      delete newValues.listItems
      delete newValues.dispatch
      return {
        ...state, 
        isModalOpen: true, 
        modalType: 'edit',
        id: newValues.id,
        title: newValues.title, 
        description: newValues.description
      }
    default:
      return state;
  }
}

async function deleteListById(id) {
  const res = await API.graphql(graphqlOperation(deleteList, { input: {id}}))
  console.log('Deleted List', res)
}

function Main() {
  const [state, dispatch] = useReducer(listReducer, initialState)

  async function fetchList() {
    const {data} = await API.graphql(graphqlOperation(listLists))
    dispatch({ type:'UPDATE_LISTS', value: data.listLists.items})
  }


  useEffect(() => {
    fetchList()
  },[]);

  useEffect(() => {
    const createListSub = 
      API.graphql(graphqlOperation(onCreateList)).subscribe({
        next: ({_provider, value}) => {
          console.log('onCreateList called')
          dispatch({ type: 'UPDATE_LISTS', value: [value.data.onCreateList]});
        },
      });

      const deleteListSub = 
      API.graphql(graphqlOperation(onDeleteList)).subscribe({
        next: ({_provider, value}) => {
          console.log('onDeleteList called')
          dispatch({ type: 'DELETE_LIST_RESULT', value: value.data.onDeleteList.id });
        },
      });

      const updateListSub = 
      API.graphql(graphqlOperation(onUpdateList)).subscribe({
        next: ({_provider, value}) => {
          console.log('onUpdateList called', value)
          dispatch({ type: 'UPDATE_LIST_RESULT', value: value.data.onUpdateList });
        },
      });

      return () => {
        createListSub.unsubscribe()
        deleteListSub.unsubscribe()
        updateListSub.unsubscribe()
      }
  }, [])

  return (
    <AmplifyAuthenticator>
      <Container style={{height: '100vh'}}>
        <AmplifySignOut />
        <Button className="floating-button" onClick={() => dispatch({ type: 'OPEN_MODAL'})}>
          <Icon name="plus" className="floating-button-icon"/>
        </Button>
        <div>
          <MainHeader />
          <Lists lists={state.lists} dispatch={dispatch} />
        </div>
      </Container>
      <ListModal state={state} dispatch={dispatch}/>
    </AmplifyAuthenticator>
  );
}

export default Main;
